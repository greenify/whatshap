.. include:: ../README.rst

=================
Table of contents
=================

.. toctree::
   :maxdepth: 2

   installation
   guide
   notes


..
   Indices and tables
   ==================

   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`
